from django.db import models


class Asset(models.Model):
    name = models.CharField()
    description = models.CharField()
    serial_number = models.CharField()
    company_serial_code = models.CharField()
    date_bought = models.DateField()


